use crate::modules::ModuleTemplate;

use std::sync::{Arc, RwLock, TryLockError};

use super::ObjectSafeModule;

#[derive(Clone)]
pub struct Input {
    value: Arc<RwLock<f32>>,
    prev_value: f32,
}

impl Input {
    pub fn new(value: Arc<RwLock<f32>>) -> ModuleTemplate<Input> {
        ModuleTemplate {
            module: Input {
                value,
                prev_value: 0.,
            },
        }
    }
}

impl ObjectSafeModule for Input {
    fn next(&mut self, _: u64) -> f32 {
        match self.value.try_read() {
            Ok(value) => {
                self.prev_value = *value;
                *value
            }
            Err(err) => match err {
                TryLockError::WouldBlock => self.prev_value,
                TryLockError::Poisoned(poison_error) => {
                    panic!("Control mutex RwLock poisoned! {:?}", poison_error)
                }
            },
        }
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
