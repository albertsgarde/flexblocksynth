use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct CustomOscillator<F: Module> {
    frequency: F,
    samples: Vec<f32>,
    inverse_sample_rate: f32,
    cur_pos: f32,
}

impl<F: Module> CustomOscillator<F> {
    pub fn new(
        frequency: impl Into<ModuleTemplate<F>>,
        samples: impl AsRef<[f32]>,
        phase: f32,
        sample_rate: u32,
    ) -> ModuleTemplate<Self> {
        let samples = samples.as_ref();
        let max_amplitude = samples
            .iter()
            .map(|&x| x.abs())
            .max_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();
        let samples: Vec<_> = samples.iter().map(|&x| x / max_amplitude).collect();

        ModuleTemplate {
            module: Self {
                frequency: frequency.into().module,
                samples,
                inverse_sample_rate: 1. / (sample_rate as f32),
                cur_pos: phase % 1.,
            },
        }
    }

    fn value(&self, cur_pos: f32) -> f32 {
        let cur_pos = cur_pos * self.samples.len() as f32;
        let ratio = cur_pos.fract();
        let cur_pos = {
            let cur_pos = cur_pos as usize;
            if cur_pos == self.samples.len() {
                0
            } else {
                cur_pos
            }
        };
        let next_pos = if cur_pos == self.samples.len() - 1 {
            0
        } else {
            cur_pos + 1
        };
        self.samples[cur_pos] * (1. - ratio) + self.samples[next_pos] * ratio
    }
}

impl<F: Module> ObjectSafeModule for CustomOscillator<F> {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.cur_pos += self.frequency.next(sample_num) * self.inverse_sample_rate;
        self.cur_pos %= 1.;
        self.value(self.cur_pos)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
