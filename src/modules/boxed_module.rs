use crate::modules::ObjectSafeModule;

use super::{Module, ModuleTemplate};

pub struct BoxedModule {
    module: Box<dyn ObjectSafeModule>,
}

impl BoxedModule {
    pub fn new<M: Module + 'static>(module: impl Into<ModuleTemplate<M>>) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Self {
                module: Box::new(module.into().module),
            },
        }
    }

    pub(super) fn from_boxed_module(module: Box<dyn ObjectSafeModule>) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Self { module },
        }
    }
}

impl Clone for BoxedModule {
    fn clone(&self) -> Self {
        Self {
            module: self.module.dyn_clone(),
        }
    }
}

impl ObjectSafeModule for BoxedModule {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.module.next(sample_num)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        self.module.dyn_clone()
    }
}
