use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct Clamp<M>
where
    M: Module,
{
    module: M,
    min: f32,
    max: f32,
}

impl<M> Clamp<M>
where
    M: Module,
{
    pub fn new(module: ModuleTemplate<M>, min: f32, max: f32) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Clamp {
                module: module.module,
                min,
                max,
            },
        }
    }
}

impl<M> ObjectSafeModule for Clamp<M>
where
    M: Module,
{
    fn next(&mut self, sample_num: u64) -> f32 {
        let value = self.module.next(sample_num);
        value.min(self.max).max(self.min)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
