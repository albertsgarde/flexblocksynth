use flexblock_synth::modules::*;
use rand::thread_rng;

fn _sfx_fractal(value: (f32, f32), initial_value: (f32, f32)) -> (f32, f32) {
    let initial_value_squared = (
        initial_value.0 * initial_value.0,
        initial_value.1 * initial_value.1,
    );
    let squared_norm = value.0 * value.0 + value.1 * value.1;
    let new_left =
        value.0 * (squared_norm - initial_value_squared.0) + value.1 * initial_value_squared.1;
    let new_right =
        value.1 * (squared_norm - initial_value_squared.0) - value.0 * initial_value_squared.1;
    (new_left, new_right)
}

fn _burning_ship_fractal(value: (f32, f32), initial_value: (f32, f32)) -> (f32, f32) {
    (
        value.0 * value.0 - value.1 * value.1 + initial_value.0,
        2. * (value.0 * value.1).abs() + initial_value.1,
    )
}

fn main() {
    let sample_rate = 44100;
    let signal_length: u32 = 20;

    for i in 0..10 {
        let synth = RandomWalk::new(thread_rng(), 0.2, 0.9, sample_rate);
        let mut synth = synth.create_instance();
        let samples: Vec<_> = (0..sample_rate * signal_length)
            .map(|i| synth.next(i as u64))
            .collect();
        let std_dev = (samples.iter().map(|x| x * x).sum::<f32>() / samples.len() as f32).sqrt();
        println!("std_dev: {}", std_dev);
        let mut file = hound::WavWriter::create(
            format!("test_{i}.wav"),
            hound::WavSpec {
                channels: 1,
                sample_rate: sample_rate as u32,
                bits_per_sample: 32,
                sample_format: hound::SampleFormat::Float,
            },
        )
        .unwrap();
        for sample in samples {
            file.write_sample(sample).unwrap();
        }
    }

    let noise = NoiseOscillator::new(thread_rng()) * 0.5;

    let mut noise_synth = noise.module();
    let noise_samples: Vec<_> = (0..sample_rate * 2)
        .map(|i| noise_synth.next(i as u64))
        .collect();
    let mut file = hound::WavWriter::create(
        "test_noise.wav",
        hound::WavSpec {
            channels: 1,
            sample_rate: sample_rate as u32,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        },
    )
    .unwrap();
    for sample in noise_samples {
        file.write_sample(sample).unwrap();
    }
}
