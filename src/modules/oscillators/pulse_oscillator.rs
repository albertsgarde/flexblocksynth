use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct PulseOscillator<F: Module, D: Module> {
    frequency: F,
    duty_cycle: D,
    inverse_sample_rate: f32,
    cur_pos: f32,
}

impl<F: Module, D: Module> PulseOscillator<F, D> {
    pub fn new(
        frequency: impl Into<ModuleTemplate<F>>,
        duty_cycle: impl Into<ModuleTemplate<D>>,
        sample_rate: u32,
    ) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Self {
                frequency: frequency.into().module,
                duty_cycle: duty_cycle.into().module,
                inverse_sample_rate: 1. / (sample_rate as f32),
                cur_pos: 0.,
            },
        }
    }
}

impl<F: Module, D: Module> ObjectSafeModule for PulseOscillator<F, D> {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.cur_pos += self.frequency.next(sample_num) * self.inverse_sample_rate;
        self.cur_pos %= 1.;
        let duty_cycle = self.duty_cycle.next(sample_num);
        if self.cur_pos < duty_cycle {
            1.
        } else {
            -1.
        }
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
