use rand::Rng;
use rand_distr::{num_traits::Pow, StandardNormal};

use crate::modules::{ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct RandomWalk<R>
where
    R: Rng + Clone + 'static,
{
    rng: R,
    gamma: f32,
    std_dev: f32,
    cur_value: f32,
}

impl<R> RandomWalk<R>
where
    R: Rng + Clone + 'static,
{
    /// Initializes a RandomWalk module.
    /// All samples have a mean of 0 and a standard deviation of `sample_std_dev`.
    /// Dampening describes how much of a sample is kept a second later.
    pub fn new(
        mut rng: R,
        sample_std_dev: f32,
        dampening: f32,
        sample_rate: u32,
    ) -> ModuleTemplate<Self> {
        let gamma = (1. - dampening).pow(1. / (sample_rate as f32));
        let std_dev = (1. - gamma * gamma).sqrt() * sample_std_dev;
        let start_value = rng.sample::<f32, _>(StandardNormal) * sample_std_dev;
        ModuleTemplate {
            module: Self {
                rng,
                gamma,
                std_dev,
                cur_value: start_value,
            },
        }
    }
}

impl<R> ObjectSafeModule for RandomWalk<R>
where
    R: Rng + Clone + 'static,
{
    fn next(&mut self, _sample_num: u64) -> f32 {
        self.cur_value =
            self.cur_value * self.gamma + self.rng.sample::<f32, _>(StandardNormal) * self.std_dev;
        self.cur_value
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
