use crate::{
    effects,
    modules::{Module, ModuleTemplate, ObjectSafeModule},
};

#[derive(Clone)]
pub struct Distortion<S: Module, A: Module, P: Module> {
    source: S,
    amplitude: A,
    power: P,
}

impl<S: Module, A: Module, P: Module> Distortion<S, A, P> {
    pub fn new(
        source: impl Into<ModuleTemplate<S>>,
        amplitude: impl Into<ModuleTemplate<A>>,
        power: impl Into<ModuleTemplate<P>>,
    ) -> ModuleTemplate<Distortion<S, A, P>> {
        ModuleTemplate {
            module: Distortion {
                source: source.into().module,
                amplitude: amplitude.into().module,
                power: power.into().module,
            },
        }
    }
}

impl<S: Module, A: Module, P: Module> ObjectSafeModule for Distortion<S, A, P> {
    fn next(&mut self, sample_num: u64) -> f32 {
        let amplitude = self.amplitude.next(sample_num);
        let sample = self.source.next(sample_num);
        let power = self.power.next(sample_num);
        effects::distortion(sample, amplitude, power)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
