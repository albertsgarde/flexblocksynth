use crate::modules::{ModuleTemplate, ObjectSafeModule};

use rand::Rng;
use rand_distr::StandardNormal;

#[derive(Clone)]
pub struct NoiseOscillator<R: Rng + Clone> {
    rng: R,
}

impl<R: Rng + Clone + 'static> NoiseOscillator<R> {
    pub fn new(rng: R) -> ModuleTemplate<NoiseOscillator<R>> {
        ModuleTemplate {
            module: NoiseOscillator { rng },
        }
    }
}

impl<R: Rng + Clone + 'static> ObjectSafeModule for NoiseOscillator<R> {
    fn next(&mut self, _: u64) -> f32 {
        self.rng.sample(StandardNormal)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
