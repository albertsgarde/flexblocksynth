mod oscillators;
use std::ops;

pub use oscillators::*;
mod effects;
pub use effects::*;
mod combinators;
pub use combinators::*;
mod envelope;
pub use envelope::*;
mod delay;
pub use delay::*;
mod sampler;
pub use sampler::*;
mod input;
pub use input::*;
mod boxed_module;
pub use boxed_module::*;
mod batched_boxed_module;
pub use batched_boxed_module::*;

use crate::modules;

#[derive(Clone)]
pub struct ModuleTemplate<M: Module> {
    module: M,
}

impl<M: Module> ModuleTemplate<M> {
    pub fn create_instance(&self) -> M {
        self.module.clone()
    }

    pub fn module(self) -> M {
        self.module
    }

    pub fn boxed(self) -> ModuleTemplate<BoxedModule> {
        BoxedModule::from_boxed_module(self.module.dyn_clone())
    }
}

impl<L: Module, R: Module> ops::Add<ModuleTemplate<R>> for ModuleTemplate<L> {
    type Output = ModuleTemplate<modules::Add<L, R>>;

    fn add(self, rhs: ModuleTemplate<R>) -> Self::Output {
        modules::Add::new(self, rhs)
    }
}

impl<L: Module, R: Module> ops::Mul<ModuleTemplate<R>> for ModuleTemplate<L> {
    type Output = ModuleTemplate<modules::Multiply<L, R>>;

    fn mul(self, rhs: ModuleTemplate<R>) -> Self::Output {
        modules::Multiply::new(self, rhs)
    }
}

impl<L: Module> ops::Add<f32> for ModuleTemplate<L> {
    type Output = ModuleTemplate<modules::Add<L, f32>>;

    fn add(self, rhs: f32) -> Self::Output {
        modules::Add::new(self, ModuleTemplate { module: rhs })
    }
}

impl<L: Module> ops::Mul<f32> for ModuleTemplate<L> {
    type Output = ModuleTemplate<modules::Multiply<L, f32>>;

    fn mul(self, rhs: f32) -> Self::Output {
        modules::Multiply::new(self, ModuleTemplate { module: rhs })
    }
}

impl std::convert::From<f32> for ModuleTemplate<f32> {
    fn from(value: f32) -> Self {
        ModuleTemplate { module: value }
    }
}

pub trait ObjectSafeModule: 'static {
    fn next(&mut self, sample_num: u64) -> f32;

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule>;
}

pub trait Module: ObjectSafeModule + Clone {}

impl<T> Module for T where T: ObjectSafeModule + Clone {}

impl ObjectSafeModule for f32 {
    fn next(&mut self, _: u64) -> f32 {
        *self
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(*self)
    }
}
