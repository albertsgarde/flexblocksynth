use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct Subtract<L: Module, R: Module> {
    lhs: L,
    rhs: R,
}

impl<L: Module, R: Module> Subtract<L, R> {
    pub fn new(lhs: ModuleTemplate<L>, rhs: ModuleTemplate<R>) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Subtract {
                lhs: lhs.module,
                rhs: rhs.module,
            },
        }
    }
}

impl<L: Module, R: Module> ObjectSafeModule for Subtract<L, R> {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.lhs.next(sample_num) - self.rhs.next(sample_num)
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
