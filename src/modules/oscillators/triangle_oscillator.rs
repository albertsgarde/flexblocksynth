use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct TriangleOscillator<F: Module> {
    frequency: F,
    inverse_sample_rate: f32,
    cur_pos: f32,
}

impl<F: Module> TriangleOscillator<F> {
    pub fn new(
        frequency: impl Into<ModuleTemplate<F>>,
        sample_rate: u32,
    ) -> ModuleTemplate<TriangleOscillator<F>> {
        ModuleTemplate {
            module: TriangleOscillator {
                frequency: frequency.into().module,
                inverse_sample_rate: 1. / (sample_rate as f32),
                cur_pos: 0.,
            },
        }
    }
}

impl<F: Module> ObjectSafeModule for TriangleOscillator<F> {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.cur_pos += self.frequency.next(sample_num) * self.inverse_sample_rate;
        self.cur_pos %= 1.;
        ((self.cur_pos - 0.5).abs() - 0.25) * 4.
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
