use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct BufferedSum<M>
where
    M: Module,
{
    modules: Vec<M>,
    buffer: Vec<f32>,
    buffer_position: usize,
}

impl<M> BufferedSum<M>
where
    M: Module,
{
    pub fn new<I>(modules: I, buffer_size: usize) -> ModuleTemplate<Self>
    where
        I: IntoIterator<Item = ModuleTemplate<M>>,
    {
        ModuleTemplate {
            module: Self {
                modules: modules
                    .into_iter()
                    .map(|template| template.module)
                    .collect(),
                buffer: vec![0.0; buffer_size],
                buffer_position: buffer_size - 1,
            },
        }
    }
}

impl<M> ObjectSafeModule for BufferedSum<M>
where
    M: Module,
{
    fn next(&mut self, sample_num: u64) -> f32 {
        self.buffer_position += 1;
        if self.buffer_position == self.buffer.len() {
            for sample in self.buffer.iter_mut() {
                *sample = 0.;
            }
            for module in self.modules.iter_mut() {
                for (i, sample) in self.buffer.iter_mut().enumerate() {
                    *sample += module.next(sample_num + i as u64);
                }
            }
            self.buffer_position = 0;
        }
        self.buffer[self.buffer_position]
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
