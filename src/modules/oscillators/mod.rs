mod custom_oscillator;
mod noise_oscillator;
mod pulse_oscillator;
mod random_walk;
mod saw_oscillator;
mod sine_oscillator;
mod triangle_oscillator;

pub use custom_oscillator::CustomOscillator;
pub use noise_oscillator::NoiseOscillator;
pub use pulse_oscillator::PulseOscillator;
pub use random_walk::RandomWalk;
pub use saw_oscillator::SawOscillator;
pub use sine_oscillator::SineOscillator;
pub use triangle_oscillator::TriangleOscillator;
