fn distortion_function(x: f32) -> f32 {
    x / (1. + x.abs())
}

pub fn distortion(sample: f32, amplitude: f32, power: f32) -> f32 {
    distortion_function(sample * power / amplitude) / distortion_function(power / amplitude)
}
