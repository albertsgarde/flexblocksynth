use crate::modules::ObjectSafeModule;

use super::{Module, ModuleTemplate};

pub struct BatchedBoxedModule {
    module: Box<dyn ObjectSafeModule>,
    buffer: Vec<f32>,
    buffer_position: usize,
}

impl BatchedBoxedModule {
    pub fn new<M: Module + 'static>(
        module: impl Into<ModuleTemplate<M>>,
        buffer_size: usize,
    ) -> ModuleTemplate<Self> {
        ModuleTemplate {
            module: Self {
                module: Box::new(module.into().module),
                buffer: vec![0.0; buffer_size],
                buffer_position: buffer_size - 1,
            },
        }
    }
}

impl Clone for BatchedBoxedModule {
    fn clone(&self) -> Self {
        Self {
            module: self.module.dyn_clone(),
            buffer: self.buffer.clone(),
            buffer_position: self.buffer_position,
        }
    }
}

impl ObjectSafeModule for BatchedBoxedModule {
    fn next(&mut self, sample_num: u64) -> f32 {
        self.buffer_position += 1;
        if self.buffer_position == self.buffer.len() {
            for (i, sample) in self.buffer.iter_mut().enumerate() {
                *sample = self.module.next(sample_num + i as u64);
            }
            self.buffer_position = 0;
        }
        self.buffer[self.buffer_position]
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
