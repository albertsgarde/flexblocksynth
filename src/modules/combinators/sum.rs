use crate::modules::{Module, ModuleTemplate, ObjectSafeModule};

#[derive(Clone)]
pub struct Sum<M>
where
    M: Module,
{
    modules: Vec<M>,
}

impl<M> Sum<M>
where
    M: Module,
{
    pub fn new<I>(modules: I) -> ModuleTemplate<Self>
    where
        I: IntoIterator<Item = ModuleTemplate<M>>,
    {
        ModuleTemplate {
            module: Self {
                modules: modules
                    .into_iter()
                    .map(|template| template.module)
                    .collect(),
            },
        }
    }
}

impl<M> ObjectSafeModule for Sum<M>
where
    M: Module,
{
    fn next(&mut self, sample_num: u64) -> f32 {
        self.modules.iter_mut().map(|m| m.next(sample_num)).sum()
    }

    fn dyn_clone(&self) -> Box<dyn ObjectSafeModule> {
        Box::new(self.clone())
    }
}
